# Bob's Petshop
## Hexagonal Architecture

This is a demo project, implementing a hypothetical pet store using hexagonal architecture.  I got a bit ahead of myself, and fairly fully fleshed all the models and use cases I implemented out before tryng to add user interaction with Symfony.

In retrospect, this was a mistake, because I ended up spending far more time debugging the infrastructure than the actual PetShop code.

## Instructions

This project was built using [DDEV](https://github.com/drud/ddev).  It should run using other docker based LAMP type stacks, but DDEV will make it fast and easy.

Since I'm on a Mac and use brew, here are the instructions for getting going with that (DDEV is also available for Windows and Linux, installation instructions are [here](https://ddev.readthedocs.io/en/stable/#docker-installation):

1. Open a terminal
1. `brew install ddev`
2. `git clone https://gitlab.com/ultrabob/petshop`
3. `cd petshop`
4. `ddev start` (to get it up and running using ssl, a nice url, and other things there are some steps to take, but this shouldn't be necessary)
4. `ddev composer install`
5. Visit the url ddev provides after it downloads the containers and spins them up
6. You are now looking at a very simple Intro page for running the demos I could get ready in time.
7. You can run the tests by running `ddev exec ./runtests.sh` from the terminal in the project folder.

An explanation of the demo is available on the page.

## Background

Hexagonal Architecture was a completely new concept for me, and I didn't feel like I quite had a handle on it from just
the blog post linked in the project.  I purchased Advanced Web Application Architecture by Matthias Noback, and read
about half of it to better understand the whole concept.

I then embarked on setting up the Domain and application with some unit tests.  I spent a fair amount of time on the
tests because it is an area I've wanted to learn, but haven't had a lot of time to work on.  I got the basics of the
Application created, and thought I had plenty of time left to set up a few controllers, maybe some console commands, and
build some good reports, but all of that took a lot longer than I had hoped, and the demo is not quite as polished as I
would like it to be.

### Note

Implemented, but not included is code to handle: 
* optioning of pets (with a verification code to validate the purchaser is the optioner)
* sales that could be returned to enable determining how many pets can safely be purchase considering possible returns.
