<?php
declare(strict_types=1);

namespace Test\Acceptance;


use DateTimeImmutable;
use PetShop\Application\CreatePet;
use PetShop\Domain\Model\Pet\Pet;
use PetShop\Domain\Model\Pet\PetAdded;
use PetShop\Domain\Model\Pet\PetId;
use PHPUnit\Framework\Assert;
use RuntimeException;

class PetAddedContext extends FeatureContext
{
    private ?PetId $petId;

    private ?string $type;
    private string $name;
    private string $description;
    private int $price;
    private DateTimeImmutable $dob;

    /**
     * @Given An animal of a specific type is created
     */
    public function anAnimalHasBeenCreated(): void
    {
        $this->type = 'dog';
        $this->name = 'Fido';
        $this->description = 'A fiercely loyal companion';
        $this->price = 78000;
        $this->dob = new DateTimeImmutable('6 months ago');
    }

    /**
     * @When there is space in the yard for the animal
     */
    public function animalIsCreated():void
    {
        Assert::assertNotNull($this->type);
        Assert::assertTrue(in_array($this->type, Pet::$available_types));

        $this->petId = $this->application()->createPet(
            new CreatePet($this->type, $this->name, $this->description, $this->price, $this->dob)
        );
    }

    /**
     * @Then the animal should be added to the yard.
     */
    public function theyShouldBeAddedToTheYard(): void
    {
        Assert::assertNotNull($this->petId);

        $events = $this->dispatchedEvents();

        foreach($events as $event) {
            echo (get_class($event) . '\n');
            if ($event instanceof PetAdded && $event->petId()->equals($this->petId)) {
                return;
            }
        }

        throw new RuntimeException('Expected a PetAdded event for the pet we added.');
    }
}
