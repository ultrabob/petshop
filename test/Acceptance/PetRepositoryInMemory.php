<?php
declare(strict_types=1);

namespace Test\Acceptance;

use Assert\Assert;
use PetShop\Domain\Model\Pet\Pet;
use PetShop\Domain\Model\Pet\PetId;
use PetShop\Domain\Model\Pet\PetRepository;
use RuntimeException;
use Ramsey\Uuid\Uuid;

class PetRepositoryInMemory implements PetRepository
{
    /**
     * @var array<Pet>
     */
    private array $pets = [];

    public function save(Pet $pet): void
    {
        $this->pets[$pet->petId()->asString()] = $pet;
    }

    /**
     * @inheritDoc
     */
    public function getById(PetId $petId): Pet
    {
        if (!isset($this->pets[$petId->asString()])) {
            throw new RuntimeException("Could not find a pet with id {$petId->asString()}");
        }

        return $this->pets[$petId->asString()];
    }

    public function exists(PetId $petId): bool
    {
        return isset($this->pets[$petId->asString()]);
    }

    public function show(PetId $petId): void
    {
        // TODO: Implement show() method.
    }

    public function returnPetToYard(PetId $petId): void
    {
        // TODO: Implement returnPetToYard() method.
    }

    public function getAll(): array
    {
        return $this->pets;
    }

    public static function nextId(): PetId
    {
        return PetId::fromString(Uuid::uuid4()->toString());
    }

    /**
     * @return int[][]
     */
    public function occupancy(): array
    {

        $locations = ['yard', 'showroom'];
        $occupancy = [];
        foreach ($locations as $location) {
            if (!isset($occupancy[$location])) {
                $occupancy[$location] = [];
            }
            foreach (Pet::$available_types as $type) {
                if (!isset($occupancy[$location][$type])) {
                    $occupancy[$location][$type] = 0;
                }
            }

        }
        foreach($this->pets as $pet) {
            $petLocation = $pet->location();
            $petType = $pet->type();
            if (!empty($petLocation) && in_array($petLocation, $locations)) {
                $occupancy[$petLocation][$petType]++;
            }
        }
        return $occupancy;
    }

    /**
     * @return int[][]
     */
    public function availability(): array
    {
        // TODO: consider returnable animals.
        $occupancy = $this->occupancy();
        $availability = [];
        foreach (Pet::$occupancy_limits as $location => $types) {
            foreach ($types as $type => $occupancy_limit) {
                if (!isset($occupancy[$location]) || !isset($occupancy[$location][$type])) {
                    $availability[$location][$type] = $occupancy_limit;
                } else {
                    $availability[$location][$type] = $occupancy_limit - $occupancy[$location][$type];
                }
            }
        }
        return $availability;
    }

    public function availablePets(): array
    {
        $availablePets = [];
        foreach ($this->pets as $pet) {
            if ($pet->isAvailable()) {
                $availablePets[$pet->petId()->asString()] = $pet->getShowablePet();
            }
        }
        return $availablePets;
    }


}
