Feature:

  When creating an animal, we only want to put the animal in the yard if
  there is space in the yard for that kind of animal

  Scenario: The PetId Matches the animal added
    Given An animal of a specific type is created
    When there is space in the yard for the animal
    Then the animal should be added to the yard.
