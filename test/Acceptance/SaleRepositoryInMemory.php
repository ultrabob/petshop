<?php


namespace Test\Acceptance;


use PetShop\Domain\Model\Sale\SaleRepository;
use PetShop\Domain\Model\Sale\Sale;
use PetShop\Domain\Model\Sale\SaleId;
use Ramsey\Uuid\Uuid;
use RuntimeException;

class SaleRepositoryInMemory implements SaleRepository
{
    /**
     * @var array<Sale>
     */
    private array $sales = [];

    public static function nextId(): SaleId
    {
        return SaleId::fromString(Uuid::uuid4()->toString());
    }

    public function save(Sale $sale): void
    {
        $this->sales[$sale->id()->asString()] = $sale;
    }

    /**
     * @inheritDoc
     */
    public function getById(SaleId $saleId): Sale
    {
        if (!isset($this->sales[$saleId->asString()])) {
            throw new RuntimeException("Could not find a sale with id {$saleId->asString()}");
        }

        return $this->sales[$saleId->asString()];
    }

    public function exists(SaleId $saleId): bool
    {
        return isset($this->sales[$saleId->asString()]);
    }

    /**
     * @inheritDoc
     */
    public function getAll(): array
    {
        return $this->sales;
    }
}
