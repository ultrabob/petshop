<?php
declare(strict_types=1);

namespace Test\Acceptance;

use PetShop\Application\EventDispatcherWithSubscribers;
use PetShop\Infrastructure\ServiceContainer;

final class ServiceContainerForAcceptanceTesting extends ServiceContainer
{
    private ?EventDispatcherWatcher $eventDispatcherWatcher = null;

    protected function registerEventSubscribers(EventDispatcherWithSubscribers $eventDispatcher): void
    {
        parent::registerEventSubscribers($eventDispatcher);

        $eventDispatcher->subscribeToAllEvents([$this->eventDispatcherWatcher(), 'notify']);

        // Test-specific listeners here;

    }

    public function eventDispatcherWatcher(): EventDispatcherWatcher
    {
        if ($this->eventDispatcherWatcher === null) {
            $this->eventDispatcherWatcher = new EventDispatcherWatcher();
        }

        return $this->eventDispatcherWatcher;
    }


}
