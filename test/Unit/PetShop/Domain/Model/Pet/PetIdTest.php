<?php


namespace Tests\Domain\Model;

use PetShop\Domain\Model\Pet\PetId;
use PHPUnit\Framework\TestCase;
use InvalidArgumentException;

class PetIdTest extends TestCase
{
    public function testPetCreation(): void
    {
        $uuid1 = $this->anIdString();
        $id = PetId::fromString($uuid1);
        $this->assertEquals($id->asString(), $uuid1);
        $this->assertInstanceOf('PetShop\Domain\Model\Pet\PetId', $id);
        $badId = 'fish';
        $this->expectException(InvalidArgumentException::class);
        $id2 = PetId::fromString($badId);
    }

    private function anIdString(): string
    {
        return 'e5017cfc-00df-40a2-aae2-e98bdcbc5e65';
    }
}
