<?php

namespace Tests\Model\Entities;

use DateTimeImmutable;
use PetShop\Domain\Model\Common\EntityTestCase;
use PetShop\Domain\Model\Pet\Pet;
use PetShop\Domain\Model\Pet\PetAddRequested;
use PetShop\Domain\Model\Pet\PetId;
use Exception;
use Ramsey\Uuid\Uuid;

class PetTest extends EntityTestCase
{
    /**
     * @throws Exception
     */
    public function testChippablePetType(): void
    {
        $now = new DateTimeImmutable('NOW');
        $name = 'Kitty';
        $dob = $this->aDate();
        $pet = $this->aChippableAnimalType();
        $pet->setShowDetails($name, 'A lovely cat', 34);
        $pet->setDob($dob);
        $this->assertFalse($pet->isAvailable());
        $pet->chip($this->aKnownPetId()->asString(), $now);
        $this->assertTrue($pet->isAvailable());
        $this->assertEquals($pet->petId(), $this->aKnownPetId());
        $this->assertInstanceOf('PetShop\Domain\Model\Pet\Pet', $pet);
        $this->assertTrue($pet->isChippableAnimal());
    }

    public function testTooYoungToSell(): void
    {
        $dob = new DateTimeImmutable('1 month ago');
        $pet = $this->aChippableAnimalType();
        $pet->setDob($dob);
        $this->assertFalse($pet->isAvailable());
        $this->assertTrue($pet->isChippableAnimal());
        $this->assertFalse($pet->isChippable());
        $this->expectException(Exception::class);
        $pet->chip($this->aKnownPetId()->asString(), new DateTimeImmutable('NOW'));
    }

    public function unChippableAnimalsUnChippable(): void
    {
        $pet = $this->unChippableAnimalType();
        $this->assertFalse($pet->isChippableAnimal());
        $this->assertFalse($pet->isChippable());
        $this->expectException(Exception::class);
        $pet->chip($this->aKnownPetId()->asString(), new DateTimeImmutable('NOW'));
    }

    public function testInvalidType(): void
    {
        $this->expectException(Exception::class);
        $pet3 = Pet::addPet($this->aKnownPetId(), 'dolphin', 'Clarence', 'Dolphin', 34, $this->aDate());
    }

    /**
     * @test
     */
    public function firesAddEvent(): void
    {
        $event_pet = Pet::addPet($this->aKnownPetId(), 'dog', 'Fido', 'Courageous Canine Companion', 78000, new DateTimeImmutable('1 month ago'));
        $this->assertEquals([
            new PetAddRequested($this->aKnownPetId(), 'dog')
        ],
            $event_pet->releaseEvents());
    }

    /**
     * @test
     */
    public function eventsAreOnlyReleasedOnce(): void
    {
        $pet = $this->unChippableAnimalType();
        $this->assertNotEmpty($pet->releaseEvents());
        $this->assertEmpty($pet->releaseEvents());
    }

    /**
     * @test
     */
    public function AvailabilityCalculations(): void
    {
        // new unchipped pet with no known dob
        $chippablePet = $this->aChippableAnimalType();
        $this->assertFalse($chippablePet->isAvailable());
        // 3 months old, but still no chip
        $chippablePet->setDob(new DateTimeImmutable('3 months ago'));
        $this->assertFalse($chippablePet->isAvailable());
        // chip it!
        $chippablePet->chip($this->aRandomId()->asString(), new DateTimeImmutable());
        $this->assertTrue($chippablePet->isAvailable());
        // has an old option on it
        $justBeyondOptionLimit = Pet::$optionableDays + 1;
        $chippablePet->option('get-me', new DateTimeImmutable("{$justBeyondOptionLimit} days ago"));
        $this->assertTrue($chippablePet->isAvailable());
        // new option
        $chippablePet->option('get-me');
        $this->assertTrue($chippablePet->isAvailable('get-me'));
        $this->assertFalse($chippablePet->isAvailable());

    }

    private function aKnownPetId(): PetId
    {
        return PetId::fromString('1ad88e6d-3696-4ddd-832b-b1055eaad84f');
    }

    private function aChippableAnimalType(PetId $petId = null): Pet
    {
        $id = $petId ?? $this->aKnownPetId();
        $type = 'cat';
        $name = 'Kitty';
        $description = 'Fiery feline furball';
        $price = 30000;
        $dob = new DateTimeImmutable('1 month ago');
        return Pet::addPet($id, $type, $name, $description, $price, $dob);

    }

    private function unChippableAnimalType(PetId $petId = null): Pet
    {
        $id = $petId ?? $this->aKnownPetId();
        $type = 'bird';
        $name = 'Birdy';
        $description = 'Brave beaked Barn Owl';
        $price = 30000;
        $dob = new DateTimeImmutable('1 month ago');
        return Pet::addPet($id, $type, $name, $description, $price, $dob);
    }

    private function aDate(): DateTimeImmutable
    {
        $date = DateTimeImmutable::createFromFormat('ymd', '190423');
        if ($date) {
            return $date;
        } else {
            throw new Exception('DateTimeImmutable creation assumption bad');
        }
    }

    private function aRandomId(): PetId
    {
        return PetId::fromString(Uuid::uuid4()->toString());
    }

}
