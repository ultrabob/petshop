<?php
declare(strict_types=1);

namespace PetShop\Domain\Model\Sale;

use PetShop\Domain\Model\Common\EntityTestCase;

class SaleTest extends EntityTestCase
{
    /**
     * @test
     */
    public function canBeCreated(): void
    {
        $id = $this->aknownSaleId();
        $petId = $this->aRandomIdString();
        $sale = new Sale(
            $this->aknownSaleId(),
            $petId,
            56
        );
        self::assertEquals($id, $sale->id());
        self::assertEquals(
            [new SaleRecorded($id, $petId)],
            $sale->releaseEvents()
        );
    }

    public function canReturn(): void
    {
        $sale = $this->aKnownSale();
        $sale->returnPet();
        self::assertEquals(
            [new SaleReturned($this->aknownSaleId(), $this->aKnownIdString())],
            $sale->releaseEvents()
        );
    }

    private function aknownSaleId(): SaleId
    {
        return SaleId::fromString($this->aKnownIdString());
    }

    private function randomSaleId(): SaleId
    {
        return SaleId::fromString($this->aRandomIdString());
    }

    private function aKnownSale(): Sale
    {
        return new Sale(
            $this->aknownSaleId(),
            $this->aKnownIdString(),
            46
            );
    }
}
