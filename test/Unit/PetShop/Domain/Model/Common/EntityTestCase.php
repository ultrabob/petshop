<?php
declare(strict_types=1);

namespace PetShop\Domain\Model\Common;

use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

abstract class EntityTestCase extends TestCase
{
    /**
     * @param string $expectedClass
     * @param array<object> $objects
     */
    protected static function assertArrayContainsObjectOfType(string $expectedClass, array $objects): void
    {
        $objectsOfExpectedType = array_filter(
            $objects,
            function ($object) use ($expectedClass) {
                return $object instanceof $expectedClass;
            });
        self::assertNotEmpty($objectsOfExpectedType, "Expected array to contain object of type {$expectedClass}");
    }

    public function aKnownIdString(): string {
        return '1ad88e6d-3696-4ddd-832b-b1055eaad84f';
    }

    public function aRandomIdString(): string {
        return Uuid::uuid4()->toString();
    }
}
