<?php
declare(strict_types=1);

namespace PetShop\Application;

use PHPUnit\Framework\TestCase;

final class EventDispatcherWithSubscribersTest extends TestCase
{
    /**
     * @test
     */
    public function subscribers_notified_of_specific_events(): void
    {
        $events = [];

        $subscriber1Called = false;
        $subscriber1 = function ($event) use (&$subscriber1Called, &$events) {
            $events[] = $event;
            $subscriber1Called = true;
        };

        $subscriber2Called = false;
        $subscriber2 = function ($event) use (&$subscriber2Called, &$events) {
            $events[] = $event;
            $subscriber2Called = true;
        };

        $subscriber3Called = false;
        $subscriber3 = function ($event) use (&$subscriber3Called, &$events) {
            $events[] = $event;
            $subscriber3Called = true;
        };

        $genericSubscriberCalled = false;
        $genericSubscriber = function ($event) use (&$genericSubscriberCalled, &$events) {
            $events[] = $event;
            $genericSubscriberCalled = true;
        };

        $eventDispatcher = new EventDispatcherWithSubscribers();
        $eventDispatcher->subscribeToSpecificEvent(TestEvent::class, $subscriber1);
        $eventDispatcher->subscribeToSpecificEvent(TestEvent::class, $subscriber2);
        $eventDispatcher->subscribeToSpecificEvent(TestEvent2::class, $subscriber3);
        $eventDispatcher->subscribeToAllEvents($genericSubscriber);

        $event = new TestEvent();
        $eventDispatcher->dispatch($event);

        $this->assertTrue($subscriber1Called);
        $this->assertTrue($subscriber2Called);
        $this->assertFalse($subscriber3Called);
        $this->assertTrue($genericSubscriberCalled);
        $this->assertEquals([$event, $event, $event], $events);
    }
}
