<?php
declare(strict_types=1);

namespace PetShop\Infrastructure;

use Assert\Assert;
use PetShop\Application\EventDispatcher;
use PetShop\Application\PetListeners;
use PetShop\Application\Application;
use PetShop\Application\EventDispatcherWithSubscribers;
use PetShop\Domain\Model\Pet\PetRepository;
use PetShop\Domain\Model\Pet\PetAddRequested;
use PetShop\Domain\Model\Sale\SaleRecorded;
use PetShop\Domain\Model\Sale\SaleRepository;
use PetShop\Infrastructure\Filebase\PetRepositoryFilebase;
use PetShop\Infrastructure\Filebase\SaleRepositoryFilebase;
use Test\Acceptance\PetRepositoryInMemory;
use Test\Acceptance\SaleRepositoryInMemory;

abstract class ServiceContainer
{
    private ?Application $application = null;
    protected ?EventDispatcher $eventDispatcher = null;
    private ?PetRepository $petRepository = null;
    private ?SaleRepository $saleRepository = null;

    /**
     * @return Application
     */
    public function application(): Application
    {
        if ($this->application === null) {
            $this->application = new Application(
                $this->petRepository(),
                $this->saleRepository(),
                $this->eventDispatcher()
            );
        }
        return $this->application;
    }

    private function petListeners(): PetListeners
    {
        return new PetListeners(
            $this->application(),
            $this->petRepository()
        );
    }

    /**
     * @return EventDispatcher
     */
    protected function eventDispatcher(): EventDispatcher
    {
        if ($this->eventDispatcher === null) {
            $this->eventDispatcher = new EventDispatcherWithSubscribers();

            $this->registerEventSubscribers($this->eventDispatcher);
        }
        Assert::that($this->eventDispatcher)->isInstanceOf(EventDispatcher::class);

        return $this->eventDispatcher;
    }

    protected function registerEventSubscribers(EventDispatcherWithSubscribers $eventDispatcher): void
    {
        $eventDispatcher->subscribeToSpecificEvent(
            PetAddRequested::class,
            [new PetListeners($this->application(), $this->petRepository()), 'whenPetAddRequested']
        );
        $eventDispatcher->subscribeToSpecificEvent(
            SaleRecorded::class,
            [new PetListeners($this->application(), $this->petRepository()), 'whenSaleWasRecorded']
        );
    }

    /**
     * @return PetRepository
     */
    public function petRepository(): PetRepository
    {
        if ($this->petRepository === null) {
            $this->petRepository = new PetRepositoryInMemory();
        }
        return $this->petRepository;
    }

    /**
     * @return SaleRepository
     */
    public function saleRepository(): SaleRepository
    {
        if($this->saleRepository === null) {
            $this->saleRepository = new SaleRepositoryInMemory();
        }
        return $this->saleRepository;
    }


}
