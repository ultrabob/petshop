<?php


namespace PetShop\Infrastructure\Symfony\Controller;

use DateTimeImmutable;
use PetShop\Application\Application;
use PetShop\Application\CreatePet;
use PetShop\Application\SellAnimal;
use PetShop\Domain\Model\Pet\PetId;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PetShop\Domain\Model\Sale\SaleId;


class FixturesController extends AbstractController
{
    private Application $application;

    /**
     * FixturesController constructor.
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    private function randPastDate(int $monthsAgo = 36): DateTimeImmutable
    {
        $older = strtotime("{$monthsAgo} months ago");
        $older = $older ?: 1562895611; // Just in case $older is false here which should never happen.
        $now = strtotime('now');
        $timestamp = random_int($older, $now);
        return (new DateTimeImmutable())->setTimestamp($timestamp);
    }


    /**
     * @Route ("/fixtures", methods={"GET"})
     */
    public function indexAction(): Response
    {
        $pets = [];
        $pets[] = new CreatePet('dog', 'Fido1', 'Courageous Canine Companion', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('cat', 'Kitty1', 'Ferociously Feline Furball', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('bird', 'Birdy1', 'Awesomely Avian Aviator', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('dog', 'Fido2', 'Courageous Canine Companion', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('cat', 'Kitty2', 'Ferociously Feline Furball', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('bird', 'Birdy2', 'Awesomely Avian Aviator', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('dog', 'Fido3', 'Courageous Canine Companion', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('cat', 'Kitty3', 'Ferociously Feline Furball', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('bird', 'Birdy3', 'Awesomely Avian Aviator', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('dog', 'Fido4', 'Courageous Canine Companion', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('cat', 'Kitty4', 'Ferociously Feline Furball', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('bird', 'Birdy4', 'Awesomely Avian Aviator', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('dog', 'Fido5', 'Courageous Canine Companion', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('cat', 'Kitty5', 'Ferociously Feline Furball', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('bird', 'Birdy5', 'Awesomely Avian Aviator', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('dog', 'Fido6', 'Courageous Canine Companion', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('cat', 'Kitty6', 'Ferociously Feline Furball', random_int(15000, 750000), $this->randPastDate());
        $pets[] = new CreatePet('bird', 'Birdy6', 'Awesomely Avian Aviator', random_int(15000, 750000), $this->randPastDate());

        foreach($pets as $pet) {
            $this->application->createPet($pet);
        }
        $pets_created = $pets;

        $this->application->chipChippablePets();

        $available_pets1 = $this->application->getAvailablePets();

        $this->sellAnimals(5, );

//        $this->sellAnimals(1, new DateTimeImmutable('2 months ago'));

        $sales = $this->application->getAllSales();
        $total = 0;
        $frozen = 0;
        $actual_revenue = 0;
        $sale_pets = [];
        $recorded_sales = [];
        foreach($sales as $sale) {
            $recorded_sales[] = $sale = $sale->readableSale();
            $total += $sale->amount();
            $frozen += $sale->returnable() ? $sale->amount() : 0;
            $actual_revenue += $sale->returnable() ? 0: $sale->amount();
            $sale_pets[] = $this->application->getPetReader(PetId::fromString($sale->petId()));
        }

        $available_pets2 = $this->application->getAvailablePets();

        return $this->render('fixtures.html.twig',[
            'pets_created' => $pets_created,
            'available_pets1' => $available_pets1,
            'sales' => $recorded_sales,
            'sale_pets' => $sale_pets,
            'total' => $total,
            'frozen' => $frozen,
            'actual_revenue' => $actual_revenue,
            'available_pets2' => $available_pets2,
        ]);
    }

    private function sellAnimals(int $howMany, DateTimeImmutable $when=null): void
    {

        $toSell = array_slice($this->application->getAvailablePets(), 0, $howMany);

        foreach ($toSell as $petId => $pet) {
            $date = $when ?? $this->randPastDate(1);
            $sellAnimal = new SellAnimal($pet->petId(), null, $date);
            $this->application->sellAnimal($sellAnimal);
        }
    }




}
