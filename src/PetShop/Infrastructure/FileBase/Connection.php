<?php

namespace PetShop\Infrastructure\Filebase;

use Filebase\Database;
use Filebase\Filesystem\FilesystemException;
use Filebase\Format\FormatInterface;


class Connection
{
    private string $dir;
    private string $backupLocation;
    private FormatInterface $format;
    private bool $cache;
    private int $cacheExpires;
    private bool $pretty;
    private bool $safeFilename;
    private bool $readOnly;
    /**
     * @var array<string, array>
     */
    private array $validation;


    /**
     * @var string[]|bool[]|int[]|array[]|FormatInterface[]
     */
    private array $config;
    public Database $db;

    /**
     * Connection constructor.
     * @param string|null $dir
     * @param string|null $backupLocation
     * @param FormatInterface|null $format
     * @param bool|null $cache
     * @param int|null $cacheExpires
     * @param bool|null $pretty
     * @param bool|null $safeFilename
     * @param bool|null $readOnly
     * @param string[]|array[]|null $validation
     * @throws FilesystemException
     */
    public function __construct(
        string $dir = null,
        string $backupLocation = null,
        FormatInterface $format = null,
        bool $cache = null,
        int $cacheExpires = null,
        bool $pretty = null,
        bool $safeFilename = null,
        bool $readOnly = null,
        array $validation = null
    )
    {
        //TODO: bring this in from config

        $overrides = [
            'dir' => $dir,
            'backupLocation' => $backupLocation,
            'format' => $format,
            'cache'=> $cache,
            'cacheExpires'=> $cacheExpires,
            'pretty' => $pretty,
            'safeFilename' => $safeFilename,
            'readOnly' => $readOnly,
            'validation' => $validation
        ];

        $this->config = [];
        foreach ($overrides as $key => $value) {
            if (!empty($value)) {
                $this->config[$key] = $value;
            }
        }

        $this->db = new Database($this->config);

    }


}
