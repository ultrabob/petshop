<?php
declare(strict_types=1);

namespace PetShop\Infrastructure\Filebase;

use Filebase\Database;
use PetShop\Domain\Model\Pet\Pet;
use PetShop\Domain\Model\Pet\PetId;
use PetShop\Domain\Model\Pet\PetRepository;
use Ramsey\Uuid\Uuid;
use RuntimeException;

class PetRepositoryFilebase implements PetRepository
{
    private Connection $connection;
    private Database $db;
    /**
     * PetRepositoryFilebase constructor.
     */
    public function __construct()
    {
        $dir = '/var/www/html/var/db/pet';
        $this->connection = new Connection($dir);
        $this->db = $this->connection->db;
    }

    public static function nextId(): PetId
    {
        return PetId::fromString(Uuid::uuid4()->toString());
    }

    public function save(Pet $pet): void
    {
        $petData = $pet->getShowablePet();
        $item = $this->db->get($pet->petId()->asString());
        $item->location = $pet->location();
        $item->name = $petData->name();
        $item->price = $petData->price();
        $item->object = json_encode(serialize($pet));
        $item->save();
    }

    /**
     * @inheritDoc
     */
    public function getById(PetId $petId): Pet
    {
        if ($this->exists($petId)) {
            $serialized_object = $this->db->get($petId->asString())->object;
            return unserialize(json_decode($serialized_object));
        } else {
            throw new RuntimeException("Could not find a pet with id {$petId->asString()}");
        }
    }

    public function exists(PetId $petId): bool
    {
        return $this->db->has($petId->asString());
    }

    /**
     * @inheritDoc
     */
    public function getAll(): array
    {
        $results = $this->db->findAll();
        $all = [];
        foreach ($results as $record) {
            $all[$record->id] = unserialize(json_decode($record->object));
        }
        return $all;
    }

    /**
     * @inheritDoc
     */
    public function availablePets(): array
    {
        $all = $this->getAll();
        $availablePets = [];
        foreach ($all as $pet) {
            /** @var Pet $pet */
            if ($pet->isAvailable()) {
                $availablePets[$pet->petId()->asString()] = $pet->getShowablePet();
            }
        }
        return $availablePets;
    }

    /**
     * @inheritDoc
     */
    public function occupancy(): array
    {
        $locations = ['yard', 'showroom'];
        $occupancy = [];
        foreach ($locations as $location) {
            if (!isset($occupancy[$location])) {
                $occupancy[$location] = [];
            }
            foreach (Pet::$available_types as $type) {
                if (!isset($occupancy[$location][$type])) {
                    $occupancy[$location][$type] = 0;
                }
            }
        }
        $all = $this->getAll();
        foreach($all as $id => $pet) {
            /** @var Pet $pet */
            $petLocation = $pet->location();
            $petType = $pet->type();
            if (!empty($petLocation) && in_array($petLocation, $locations)) {
                $occupancy[$petLocation][$petType]++;
            }
        }
        return $occupancy;
    }

    /**
     * @inheritDoc
     */
    public function availability(): array
    {
        // TODO: consider returnable animals.
        $occupancy = $this->occupancy();
        $availability = [];
        foreach (Pet::$occupancy_limits as $location => $types) {
            foreach ($types as $type => $occupancy_limit) {
                if (!isset($occupancy[$location]) || !isset($occupancy[$location][$type])) {
                    $availability[$location][$type] = $occupancy_limit;
                } else {
                    $availability[$location][$type] = $occupancy_limit - $occupancy[$location][$type];
                }
            }
        }
        return $availability;
    }
}
