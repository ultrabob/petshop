<?php
declare(strict_types=1);

namespace PetShop\Infrastructure\Filebase;

use Filebase\Database;
use Filebase\Filesystem\FilesystemException;
use PetShop\Domain\Model\Sale\Sale;
use PetShop\Domain\Model\Sale\SaleId;
use PetShop\Domain\Model\Sale\SaleRepository;
use Ramsey\Uuid\Uuid;
use RuntimeException;

class SaleRepositoryFilebase implements SaleRepository
{
    private Connection $connection;
    private Database $db;

    /**
     * PetRepositoryFilebase constructor.
     * @throws FilesystemException
     */
    public function __construct()
    {
        $dir = '/var/www/html/var/db/sale';
        $backupLocation = '/var/www/html/var/db/backup/sale';
        $this->connection = new Connection($dir, $backupLocation);
        $this->db = $this->connection->db;
    }


    public static function nextId(): SaleId
    {
        return SaleId::fromString(Uuid::uuid4()->toString());
    }

    public function save(Sale $sale): void
    {
        $item = $this->db->get($sale->id()->asString());
        $item->petId = $sale->Id()->asString();
        $item->object = $sale;
        $item->save();
    }

    /**
     * @inheritDoc
     */
    public function getById(SaleId $saleId): Sale
    {
        if ($this->exists($saleId)) {
            return $this->db->get($saleId->asString())->object;
        } else {
            throw new RuntimeException("Could not find a sale with id {$saleId->asString()}");
        }
    }

    public function exists(SaleId $saleId): bool
    {
        return $this->db->has($saleId->asString());
    }

    /**
     * @inheritDoc
     */
    public function getAll(): array
    {
        $results = $this->db->findAll();
        $all = [];
        foreach ($results as $record) {
            $all[$record->id] = $record->object;
        }
        return $all;
    }
}
