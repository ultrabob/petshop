<?php


namespace PetShop\Application;

use PetShop\Domain\Model\Pet\Pet;
use PetShop\Domain\Model\Pet\PetId;
use PetShop\Domain\Model\Pet\PetRepository;
use PetShop\Domain\Model\Pet\ShowablePet;
use PetShop\Domain\Model\Sale\SaleId;
use PetShop\Domain\Model\Sale\SaleReader;
use PetShop\Domain\Model\Sale\SaleRepository;
use PetShop\Domain\Model\Sale\Sale;
use RuntimeException;

final class Application
{
    private PetRepository $petRepository;

    private SaleRepository $saleRepository;

    private EventDispatcher $eventDispatcher;

    /**
     * Application constructor.
     * @param PetRepository $petRepository
     * @param SaleRepository $saleRepository
     * @param EventDispatcher $eventDispatcher
     */
    public function __construct(
        PetRepository $petRepository,
        SaleRepository $saleRepository,
        EventDispatcher $eventDispatcher
    )
    {
        $this->petRepository = $petRepository;
        $this->saleRepository = $saleRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function createPet(CreatePet $command): PetId
    {
        $petId = $this->petRepository->nextId();
        $pet = Pet::addPet(
            $petId,
            $command->type(),
            $command->name(),
            $command->description(),
            $command->price(),
            $command->dob()
        );
        $this->petRepository->save($pet);
        $this->eventDispatcher->dispatchAll($pet->releaseEvents());
        return $pet->petId();
    }

    public function chipChippablePets(): void
    {
        $pets = $this->petRepository->getAll();
        foreach ($pets as $pet) {
            /** @var Pet $pet */
            if ($pet->isChippable()) {
                $pet->chip($this->petRepository->nextId()->asString());
                $this->petRepository->save($pet);
            }
        }
    }

    /**
     * @return array<Sale>
     */
    public function getAllSales(): array
    {
        return $this->saleRepository->getAll();
    }

    public function getPetReader(PetId $petId): ShowablePet
    {
        return $this->petRepository->getById($petId)->getShowablePet();
    }

    /**
     * @return array<ShowablePet>
     */
    public function getAvailablePets(): array
    {
        return $this->petRepository->availablePets();
    }

    public function addPet(PetId $petId): void {
        $pet = $this->petRepository->getById($petId);
        $pet->yard();
        $this->petRepository->save($pet);
        $this->eventDispatcher->dispatchAll($pet->releaseEvents());
    }

    public function getSaleId(): SaleId
    {
        return $this->saleRepository::nextId();
    }

    public function sellAnimal(SellAnimal $command): void {
        $pet = $this->petRepository->getById($command->petId());
        if (!$pet->isAvailable($command->optionCode())) {
            throw new RuntimeException("Requested animal ({$command->petId()->asString()}) is unavailable for sale");
        }
        $readablePet = $pet->getShowablePet();
        $price = $readablePet->price();
        $sale = new Sale(
            $this->saleRepository::nextId(),
            $readablePet->petId()->asString(),
            $price,
            $command->saleDate()
        );
        $this->saleRepository->save($sale);
        $this->eventDispatcher->dispatchAll($sale->releaseEvents());
    }


}
