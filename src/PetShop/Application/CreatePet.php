<?php
declare(strict_types=1);

namespace PetShop\Application;

use DateTimeImmutable;

class CreatePet
{
    private string $type;
    private string $name;
    private string $description;
    private int $price;
    private \DateTimeImmutable $dob;

    /**
     * addPet constructor.
     * @param string $type
     */
    public function __construct(string $type, string $name, string $description, int $price, DateTimeImmutable $dob)
    {
        $this->type = $type;
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        $this->dob = $dob;
    }

    public function type(): string {
        return $this->type;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function price(): int
    {
        return $this->price;
    }

    /**
     * @return DateTimeImmutable
     */
    public function dob(): DateTimeImmutable
    {
        return $this->dob;
    }
}
