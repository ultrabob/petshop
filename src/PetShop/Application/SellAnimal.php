<?php
declare(strict_types=1);

namespace PetShop\Application;


use DateTimeImmutable;
use PetShop\Domain\Model\Pet\PetId;

/**
 * Class SellAnimal Command
 * @package PetShop\Application
 */
class SellAnimal
{
    private PetId $petId;
    private ?string $optionCode;
    private ?DateTimeImmutable $saleDate;

    /**
     * SellAnimal constructor.
     * @param PetId $petId
     * @param string|null $optionCode
     * @param null|DateTimeImmutable|null $saleDate
     */
    public function __construct(PetId $petId, ?string $optionCode = null, ?DateTimeImmutable $saleDate = null)
    {
        $this->petId = $petId;
        $this->optionCode = $optionCode;
        $this->saleDate = $saleDate;
    }

    /**
     * @return PetId
     */
    public function petId(): PetId
    {
        return $this->petId;
    }

    /**
     * @return string|null
     */
    public function optionCode(): ?string
    {
        return $this->optionCode;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function saleDate(): ?DateTimeImmutable
    {
        return $this->saleDate;
    }
}
