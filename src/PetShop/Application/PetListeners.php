<?php
declare(strict_types=1);

namespace PetShop\Application;

use PetShop\Domain\Model\Pet\PetAddRequested;
use PetShop\Domain\Model\Pet\PetId;
use PetShop\Domain\Model\Pet\PetRepository;
use PetShop\Domain\Model\Sale\SaleRecorded;
use RuntimeException;

final class PetListeners
{
    /**
     * @var Application
     */
    private Application $application;

    private PetRepository $petRepository;

    /**
     * PetListeners constructor.
     * @param Application $application
     */
    public function __construct(Application $application, PetRepository $petRepository)
    {
        $this->application = $application;
        $this->petRepository = $petRepository;
    }

    public function whenPetAddRequested(PetAddRequested $event): void
    {
        if (!$this->petRepository->availability()['yard'][$event->type()] > 0) {
        }
        $this->application->addPet($event->petId());

    }

    public function whenSaleWasRecorded(SaleRecorded $event): void
    {
        $pet = $this->petRepository->getById($event->petId());
        $pet->recordSale($event->saleId());
    }
}
