<?php
declare(strict_types=1);

namespace PetShop\Application;

class addPet
{
    private string $type;

    /**
     * addPet constructor.
     * @param string $type
     */
    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public function type(): string {
        return $this->type;
    }
}
