<?php
declare(strict_types=1);

namespace PetShop\Application;

use PetShop\Domain\Model\Pet\PetId;


final class showPet
{
    private PetId $petId;
    private string $type;

    /**
     * showPet constructor.
     * @param PetId $petId
     */
    public function __construct(PetId $petId, string $type)
    {
        $this->petId = $petId;
        $this->type = $type;
    }

    /**
     * @return PetId
     */
    public function petId(): PetId
    {
        return $this->petId;
    }


}
