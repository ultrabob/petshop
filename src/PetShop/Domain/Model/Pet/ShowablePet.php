<?php


namespace PetShop\Domain\Model\Pet;


use DateTimeImmutable;

class ShowablePet
{
    private PetId $petId;

    private string $type;

    private string $location;

    private string $name;

    private string $description;

    private int $price;

    private DateTimeImmutable $dob;

    private bool $chipped;

    /**
     * ShowablePet constructor.
     * @param PetId $petId
     * @param string $type
     * @param string $location
     * @param string $name
     * @param string $description
     * @param int $price
     */
    public function __construct(PetId $petId, string $type, string $location, string $name, string $description, int $price, DateTimeImmutable $dob, bool $chipped)
    {
        $this->petId = $petId;
        $this->type = $type;
        $this->location = $location;
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        $this->dob = $dob;
        $this->chipped = $chipped;
    }

    /**
     * @return PetId
     */
    public function petId(): PetId
    {
        return $this->petId;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function location(): string
    {
        return $this->location;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function price(): int
    {
        return $this->price;
    }

    /**
     * @return DateTimeImmutable
     */
    public function dob(): DateTimeImmutable
    {
        return $this->dob;
    }

    /**
     * @return bool
     */
    public function isChipped(): bool
    {
        return $this->chipped;
    }


}
