<?php


namespace PetShop\Domain\Model\Pet;


use PetShop\Domain\Model\Sale\SaleId;

class PetSold
{
    private ShowablePet $pet;
    private SaleId $saleId;

    /**
     * PetSold constructor.
     * @param ShowablePet $pet
     * @param SaleId $saleId
     */
    public function __construct(ShowablePet $pet, SaleId $saleId)
    {
        $this->pet = $pet;
        $this->saleId = $saleId;
    }

    /**
     * @return ShowablePet
     */
    public function pet(): ShowablePet
    {
        return $this->pet;
    }

    /**
     * @return SaleId
     */
    public function saleId(): SaleId
    {
        return $this->saleId;
    }
}
