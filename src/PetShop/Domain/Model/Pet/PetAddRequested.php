<?php
declare(strict_types=1);

namespace PetShop\Domain\Model\Pet;


class PetAddRequested
{
    private PetId $petId;

    private string $type;

    /**
     * PetAddRequested constructor.
     * @param PetId $petId
     * @param string $type
     */
    public function __construct(PetId $petId, string $type)
    {
        $this->petId = $petId;
        $this->type = $type;
    }

    /**
     * @return PetId
     */
    public function petId(): PetId
    {
        return $this->petId;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->type;
    }


}
