<?php
declare(strict_types=1);

namespace PetShop\Domain\Model\Pet;

use Assert\Assert;
use DateInterval;
use DateTimeImmutable;
use PetShop\Domain\Model\Common\Entity;
use PetShop\Domain\Model\Sale\SaleId;
use RuntimeException;

final class Pet {
    use Entity;
    /** @var PetId */
    private PetId $id;

    private string $name;

    private DateTimeImmutable $dob;

    private string $type;

    private string $chipId;

    private ?DateTimeImmutable $chip_date;

    private int $price;

    private SaleId $saleId;

    // one of yard, showroom, sold, or none
    private string $location;

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function location(): string
    {
        return $this->location;
    }

    private string $description;

    private DateTimeImmutable $optionedDate;

    private string $optionCode;

    // @todo these should be loaded from config
    /**
     * @var array<string>
     */
    public static array $available_types = [
        'cat',
        'dog',
        'bird'
    ];

    /**
     * @var array<string>
     */
    public static array $chipped_types = [
        'cat',
        'dog',
    ];

    /** @var array|int[][]  */
    public static array $occupancy_limits = [
        'yard' => [
            'bird' => 30,
            'cat' => 30,
            'dog' => 15,
        ],
        'showroom' => [
            'bird' => 15,
            'cat' => 10,
            'dog' => 5,
        ],
    ];

    public static int $optionableDays = 7;

    /**
     * Pet constructor.
     */
    public function __construct()
    {
        $this->location = 'none';
    }

    public function getShowablePet(): ShowablePet
    {
        return new ShowablePet(
            $this->id,
            $this->type,
            $this->location,
            $this->name,
            $this->description,
            $this->price,
            $this->dob,
            (!empty($this->chipId))
        );
    }

    /**
     * @return PetId
     */
    public function petId() : PetId
    {
        return $this->id;
    }

    public function setShowDetails(string $name, string $description, int $price):void
    {
        Assert::that($price)->greaterThan(0, 'Price must be a positive integer.');
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
    }


    public static function addPet(PetId $petId, string $type, string $name, string $description, int $price, DateTimeImmutable $dob): Pet
    {
        Assert::that($type)->inArray(self::$available_types, "'{$type}' pet type is not handled by this Pet Shop.");
        $pet = new self();
        $pet->id = $petId;
        $pet->type = $type;
        $pet->name = $name;
        $pet->description = $description;
        $pet->price = $price;
        $pet->dob = $dob;

        $pet->events[] = new PetAddRequested($petId, $type);
        return $pet;
    }


    public function isChippableAnimal(): bool
    {
        return in_array($this->type, $this::$chipped_types);
    }

    public function isChippable(): bool
    {
        if (!$this->isChippableAnimal()) {
            return false;
        }
        return $this->ageInMonths() >= 2;
    }

    public function age(): DateInterval
    {
        return $this->dob->diff(new DateTimeImmutable());
    }

    public function ageInMonths(): int
    {
        return $this->age()->y * 12 + $this->age()->m;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function option(string $optionCode, ?DateTimeImmutable $optionDate = null): void
    {
        // @todo being available for option may not be the same as sale (young animal)
        Assert::that($this->isAvailable())->true('This animal is not available to option.');
        Assert::that($optionCode)->notEmpty('The option code cannot be empty');
        $this->optionCode = $optionCode;
        $this->optionedDate = $optionDate ?? new DateTimeImmutable();
    }

    public function isOptioned(): bool
    {
        $optioned = false;
        if (isset($this->optionedDate)) {

            $optionDaysOld = $this->optionedDate->diff(new DateTimeImmutable())->format('%a');

            if ($optionDaysOld < $this::$optionableDays) {
                $optioned = true;
            }
        }
        return $optioned;
    }

    public function optionCodeMatches(string $optionCode): bool
    {
        $matches = false;
        if (isset($this->optionCode)) {
            $matches = $this->optionCode == $optionCode;
        }
        return $matches;
    }

    public function isAvailable(?string $optionCode = null): bool {
        $needsChipped = false;
        if ($this->isChippableAnimal() && !isset($this->chip_date)) {
            $needsChipped = true;
        }
        $optioned = $this->isOptioned();
        $hasPrice = !empty($this->price);

        return ($needsChipped == false &&
            empty($this->saleId) &&
            (!$optioned || (!empty($optionCode) && $this->optionCodeMatches($optionCode))) &&
            $hasPrice);
    }

    /**
     * @param DateTimeImmutable $dob
     */
    public function setDob(DateTimeImmutable $dob): void
    {
        $this->dob = $dob;
    }

    /**
     * @param string $chipId
     * @param DateTimeImmutable|null $chip_date
     * @throws RuntimeException
     */
    public function chip(string $chipId, ?DateTimeImmutable $chip_date = null): void
    {
        if (!$this->isChippableAnimal()) {
            throw new RuntimeException("{$this->type} is not a chippable animal.");
        }
        if (!$this->isChippable()) {
            throw new RuntimeException("Animal not old enough to chip.");
        }
        $this->chipId = $chipId;
        $this->chip_date = $chip_date ?? new DateTimeImmutable();
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @todo Respond to sale event
     * @param SaleId $saleId
     */
    public function recordSale(SaleId $saleId): void
    {
        $this->saleId = $saleId;
        $this->location = 'sold';
        $this->events[] = new PetSold($this->getShowablePet(), $saleId);
    }

    /**
     * @todo respond to petShown
     */
    private function show(): void
    {
        $this->location = 'showroom';
    }

    /**
     * @todo respond to returnPetToYard event
     */
    public function yard(): void
    {
        $new = $this->location == 'none';
        $this->location = 'yard';
        if ($new) {
            $this->events[] = new PetAdded($this->id, $this->type);
        }
    }
}
