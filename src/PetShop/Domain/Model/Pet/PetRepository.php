<?php
declare(strict_types=1);

namespace PetShop\Domain\Model\Pet;

use RuntimeException;

interface PetRepository
{
    public static function nextId(): PetId;

    public function save(Pet $pet): void;

    /**
     * @param PetId $petId
     * @return Pet
     * @throws RuntimeException
     */
    public function getById(PetId $petId): Pet;

    public function exists(PetId $petId): bool;

    /**
     * @return array<object>
     */
    public function getAll(): array;

    /**
     * Return a list of all available Pets using the ShowablePet DTO
     * @return array<ShowablePet>
     */
    public function availablePets(): array;

    /**
     * Return an array of how many animals of each type are occupying each location
     * @return int[][]
     */
    public function occupancy(): array;

    /**
     * @return int[][]
     */
    public function availability(): array;
}
