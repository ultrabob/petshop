<?php


namespace PetShop\Domain\Model\Sale;


use DateTimeImmutable;

class SaleReader
{

private string $saleId;
private DateTimeImmutable $saleDate;
private DateTimeImmutable $returnBy;
private int $amount;
private string $petId;

    /**
     * SaleReader constructor.
     * @param string $saleId
     * @param DateTimeImmutable $saleDate
     * @param DateTimeImmutable $returnBy
     * @param int $amount
     * @param string $petId;
     */
    public function __construct(string $saleId, DateTimeImmutable $saleDate, DateTimeImmutable $returnBy, int $amount, string $petId)
    {
        $this->saleId = $saleId;
        $this->saleDate = $saleDate;
        $this->amount = $amount;
        $this->petId = $petId;
        $this->returnBy = $returnBy;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->saleId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function date(): DateTimeImmutable
    {
        return $this->saleDate;
    }

    /**
     * @return int
     */
    public function amount(): int
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function petId(): string
    {
        return $this->petId;
    }

    public function returnBy(): DateTimeImmutable
    {
        return $this->returnBy;
    }

    public function returnable(): bool
    {
        return $this->returnBy > new DateTimeImmutable('now');
    }

}
