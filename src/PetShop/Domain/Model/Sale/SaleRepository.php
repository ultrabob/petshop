<?php

declare(strict_types=1);

namespace PetShop\Domain\Model\Sale;

use PetShop\Domain\Model\Sale\Sale;
use PetShop\Domain\Model\Sale\SaleId;
use RuntimeException;

interface SaleRepository
{

    public static function nextId(): SaleId;

    public function save(Sale $sale): void;

    /**
     * @param SaleId $saleId
     * @return Sale
     * @throws RuntimeException
     */
    public function getById(SaleId $saleId): Sale;

    public function exists(SaleId $saleId): bool;

    /**
     * @return array<Sale>
     */
    public function getAll(): array;
}
