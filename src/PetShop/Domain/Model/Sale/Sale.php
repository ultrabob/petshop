<?php
/**
 * One sale of one animal.  We do not support transactions of more than one animal
 * at this time.
 */


namespace PetShop\Domain\Model\Sale;


use DateInterval;
use DateTimeImmutable;
use PetShop\Domain\Model\Common\Entity;
use PetShop\Domain\Model\Pet\PetId;
use RuntimeException;

class Sale
{
    use Entity;
    private SaleId $saleId;
    private DateTimeImmutable $saleDate;
    private int $saleAmount;
    private string $petId;
    private DateTimeImmutable $returnBy;
    private bool $returned;
    // @todo move to config
    public static int $returnableDays = 14;

    /**
     * Sale constructor.
     * @param SaleId $saleId
     * @param string $petId
     * @param int $saleAmount
     * @param DateTimeImmutable|null $saleDate
     */
    public function __construct(SaleId $saleId, string $petId, int $saleAmount, ?DateTimeImmutable $saleDate = null)
    {
        $this->saleId = $saleId;
        $this->saleDate = $saleDate ?? new DateTimeImmutable();
        $this->petId = $petId;
        $this->saleAmount = $saleAmount;
        $this->returnBy = $this->saleDate->add(new DateInterval('P2W'));
        $this->returned = false;
        $this->events[] = new SaleRecorded($this->saleId, $this->petId);
    }

    public function returnPet(): void
    {
        if (!$this->returnable()) {
            throw new RuntimeException("Sale {$this->saleId->asString()} is out of the return period");
        }
        $this->returned = true;
        $events[] = new SaleReturned($this->saleId, $this->petId);
    }

    public function id(): SaleId
    {
        return $this->saleId;
    }

    public function returnable(): bool
    {
        return $this->returnBy > new DateTimeImmutable('now');
    }

    public function readableSale(): SaleReader
    {
        return new SaleReader($this->saleId->asString(), $this->saleDate, $this->returnBy, $this->saleAmount, $this->petId);
    }
}
