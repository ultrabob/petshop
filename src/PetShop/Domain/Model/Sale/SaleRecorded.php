<?php


namespace PetShop\Domain\Model\Sale;

use PetShop\Domain\Model\Pet\PetId;

/**
 * SaleRecorded Event
 * @package PetShop\Domain\Model\Sale
 */
class SaleRecorded
{
    private SaleId $saleId;
    private PetId $petId;

    /**
     * SaleRecorded constructor.
     * @param SaleId $saleId
     * @param string $petId
     */
    public function __construct(SaleId $saleId, string $petId)
    {
        $this->saleId = $saleId;
        $this->petId = PetId::fromString($petId);
    }

    /**
     * @return SaleId
     */
    public function saleId(): SaleId
    {
        return $this->saleId;
    }

    /**
     * @return PetId
     */
    public function petId(): PetId
    {
        return $this->petId;
    }
}
