<?php
declare(strict_types=1);

namespace PetShop\Domain\Model\Sale;

use PetShop\Domain\Model\Common\ItemId;

class SaleId
{
    use ItemId;
}

