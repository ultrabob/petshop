<?php


namespace PetShop\Domain\Model\Sale;

use PetShop\Domain\Model\Pet\PetId;

/**
 * SaleReturned Event
 * @package PetShop\Domain\Model\Sale
 */
class SaleReturnRequested
{
    private SaleId $saleId;
    private PetId $petId;

    /**
     * SaleReturned constructor.
     * @param SaleId $saleId
     * @param PetId $petId
     */
    public function __construct(SaleId $saleId, PetId $petId)
    {
        $this->saleId = $saleId;
        $this->petId = $petId;
    }

    /**
     * @return SaleId
     */
    public function saleId(): SaleId
    {
        return $this->saleId;
    }

    /**
     * @return PetId
     */
    public function petId(): PetId
    {
        return $this->petId;
    }


}
