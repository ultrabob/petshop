<?php


namespace PetShop\Domain\Model\Sale;

use PetShop\Domain\Model\Pet\PetId;

/**
 * SaleReturned Event
 * @package PetShop\Domain\Model\Sale
 */
class SaleReturned
{
    private SaleId $saleId;
    private PetId $petId;

    /**
     * SaleReturned constructor.
     * @param SaleId $saleId
     * @param string $petId
     */
    public function __construct(SaleId $saleId, string $petId)
    {
        $this->saleId = $saleId;
        $this->petId = PetId::fromString($petId);
    }

    /**
     * @return SaleId
     */
    public function saleId(): SaleId
    {
        return $this->saleId;
    }

    /**
     * @return PetId
     */
    public function petId(): PetId
    {
        return $this->petId;
    }


}
