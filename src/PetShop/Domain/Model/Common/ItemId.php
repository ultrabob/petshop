<?php
declare(strict_types=1);

namespace PetShop\Domain\Model\Common;

use Assert\Assert;

trait ItemId
{
    private string $id;

    private function __construct(string $id)
    {
        Assert::that($id)->uuid();

        $this->id = $id;
    }

    public static function fromString(string $idString): self
    {
        return new self($idString);
    }

    public function asString(): string
    {
        return $this->id;
    }

    public function equals(self $other): bool
    {
        return $this->id === $other->id;
    }
}
